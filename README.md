# 60 Days of Learning 2024

This repository will be log for my personal progress and the resources I use during my learning journey with [leapfrog](https://www.lftechnology.com/).

## Table of Contents

- [Introduction](#introduction)
- [Day Index](#day-index)
- [Resources](#resources)

## Day Index

- [Day 1](#day-01---june-01-2024)
- [Day 2](#day-02---june-02-2024)
- [Day 3](#day-03---june-03-2024)
- [Day 4](#day-04---june-04-2024)
- [Day 6](#day-06---june-06-2024)
- [Day 7](#day-07---june-07-2024)
- [Day 8](#day-08---june-08-2024)
- [Day 9](#day-09---june-09-2024)
- [Day 10](#day-10---june-10-2024)
- [Day 11](#day-11---june-11-2024)
- [Day 12](#day-12---june-12-2024)
- [Day 13](#day-13---june-13-2024)
- [Day 14](#day-14---june-14-2024)
- [Day 15](#day-15---june-15-2024)
- [Day 16](#day-16---june-17-2024)
- [Day 17](#day-17---june-18-2024)
- [Day 18](#day-18---june-19-2024)
- [Day 21](#day-21---june-21-2024)

## Introduction

I have a basic knowledge of Linux and Shell scripting, but there is many still much more for me to learn. I will be following roadmap of [roadmap.sh/linux](https://roadmap.sh/linux).

## Day 01 - June 01, 2024

Today I learn about file Archiving and Compressing.And wrote small script [ext](./scripts/ext) which extract compressed file(.tar,.tar.gz,.zip,rar).

### Note

- -z check the string is empty or not.
- "${2:-.}" check second argument if not provided use current directory as default

## Day 02 - June 02, 2024

Today I explore more about Archiving and Compressing.And wrote another script [compress](./scripts/compress) which compress the selected file and folder.

### Note

- gizp and zip use same compression algorithm with different implementation of DEFLATE compression algorithm."[link](https://dev.to/biellls/compression-clearing-the-confusion-on-zip-gzip-zlib-and-deflate-15g1#:~:text=what%20I%20learned.-,DEFLATE,same%20lossless%20data%20compression%20algorithm.)"

## Day 03 - June 03, 2024

Today, I learn about soft and hard links.

### Note

- I linux every files have Index Node(Inode).
- Soft links also know as symbolic link create new Inode. If original file is deleted then link will not work.
- Hard links uses Inode of original file. If original file is deleted then link still show the content.

## Day 04 - June 04, 2024

Today, I learn about Stdin/Stdout/stderr, cut and paste command in linux.

## Day 06 - June 06, 2024

Today, I learn to create, update and delete users and groups in linux.

## Day 07 - June 07, 2024

Today, I learned about loops in shell scripting and created a simple [game](./scripts/game) with the implementation of a while loop.

## Day 08 - June 08, 2024

I was unable to learn any thing because I don't have access to my Laptop.

## Day 09 - June 09, 2024

Today, I learned to check service status and starting and stopping service.

## Day 10 - June 10, 2024

Today, I learned about pipe, sort and grep command.

## Day 11 - June 11, 2024

Today, I learned about join and unique command.

## Day 12 - June 12, 2024

Today, I learned about awk command.

## Day 13 - June 13, 2024

Today, I install docker and learn about docker image and docker containers.

## Day 14 - June 14, 2024

Today, I learn about Dockerfile which can automatically build docker image.

## Day 15 - June 15, 2024

Today, I learn about Docker compose file which can be use to defines multi-container application.

## Day 16 - June 17, 2024

Today, I learn about Docker Layer Caching.

## Day 17 - June 18, 2024

Today, I learn about Image Tagging Best Practices and dockerhub.

## Day 18 - June 19, 2024

Today, I create a [Dockerfile](./docker-demo/Dockerfile) which use nginx.

## Day 21 - June 21, 2024

Today, I learn about docker exec command. Pull ollama image and use exec to execute ollama run llame3.

## Resources

- [Youtube-Docker](https://www.youtube.com/watch?v=pg19Z8LL06w)
- [Roadmap](https://roadmap.sh/linux)
- [Medium](https://medium.com/@cuncis/linux-archiving-commands-a-beginners-guide-to-efficient-file-management-8448f7403e95)
- [RedHat](https://www.redhat.com/sysadmin/linking-linux-explained)
- [Docker](https://docs.docker.com/guides/getting-started/get-docker-desktop/)
